## Steps to follow ⏬

## #Jenkins Installation

- https://www.jenkins.io/doc/tutorials/tutorial-for-installing-jenkins-on-AWS/

## #IAM setup
1. Create IAM user with programmatic access  if Jenkins runs on out of AWS , not needed if you are deploying the infra through **CF templates**
2. Generates access and secret access key
3. ![](Assets/InkedIAM_LI.jpg)

## #Spot requests 

![](Assets/spot1.png)
![](Assets/spot2.png)

> **Install java through user data and attach SG's**



![](Assets/Inkedspot3_LI.jpg)

![](Assets/spot4.png)

## #EC2 Spot plugin on Jenkins

![](Assets/jenkins plug.png)
![](Assets/plug2.png)

## #Configure Plugin

[http://<jenkins_url>:8080/configureClouds/](http://<jenkins>:8080/configureClouds/)

## #Add cloud 
![](Assets/cloud1.png)
![](Assets/cloud2.png)

## #Configure the JOB to run on the node
![](Assets/configurejob1.png)
![](Assets/Inkedconfigure2_LI.png)

## #Console Out

![](Assets/output.png)

## #Cleanup Env.
![](Assets/clean.png)

***##References***

[# Amazon EC2 Spot Instances](https://www.youtube.com/watch?v=tny8N-hkCqs)

[AWS Spot FAQ](https://aws.amazon.com/ec2/faqs/?s_kwcid=AL!4422!3!517651795618!p!!g!!ec2%20spot%20instances&ef_id=EAIaIQobChMI4cT41Zy_8wIVFDUrCh3UAwYQEAAYASABEgKVj_D_BwE:G:s&s_kwcid=AL!4422!3!517651795618!p!!g!!ec2%20spot%20instances#Spot_instances/?trkCampaign=acq_paid_search_brand&sc_channel=PS&sc_campaign=acquisition_IN&sc_publisher=Google&sc_category=Cloud%20Computing&sc_country=IN&sc_geo=APAC&sc_outcome=acq&sc_detail=ec2%20spot%20instances&sc_content=%7Badgroup%7D&sc_matchtype=p&sc_segment=517651795618&sc_medium=ACQ-P%7CPS-GO%7CBrand%7CDesktop%7CSU%7CCloud%20Computing%7CEC2%20Spot%7CIN%7CEN%7CSitelink)

> [EC2 Spot labs](https://ec2spotworkshops.com/launching_ec2_spot_instances/asg.html)

